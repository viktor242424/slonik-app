import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }   from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PahomComponent } from './Components/pahom/pahom.component';
import { BratishkaComponent } from './Components/bratishka/bratishka.component';
import { GaupvahtaComponent } from './Components/gaupvahta/gaupvahta.component';


// определение маршрутов
const appRoutes: Routes = [
  { path: '', component: GaupvahtaComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PahomComponent,
    BratishkaComponent,
    GaupvahtaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
