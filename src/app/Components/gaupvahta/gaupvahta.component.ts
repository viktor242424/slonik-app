import { Component, OnInit } from '@angular/core';  
import {User} from '../../models/user';
import { HttpService} from '../../service/http.service';


@Component({
  selector: 'app-gaupvahta',
  templateUrl: './gaupvahta.component.html',
  styleUrls: ['./gaupvahta.component.css'],
  providers: [HttpService]
})
export class GaupvahtaComponent implements OnInit {

  user: User;
 
  constructor(private httpService: HttpService){}
    
  ngOnInit(){
    this.httpService.getData().subscribe((response:User) => {
      this.user = response["userList"];
    });
  }
  clicks:number = 0;
  onChanged(increased:any){
      increased==true?this.clicks++:this.clicks--;
  }
  
 age:number = 24;

}
