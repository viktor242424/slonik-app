import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaupvahtaComponent } from './gaupvahta.component';

describe('GaupvahtaComponent', () => {
  let component: GaupvahtaComponent;
  let fixture: ComponentFixture<GaupvahtaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaupvahtaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaupvahtaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
