import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BratishkaComponent } from './bratishka.component';

describe('BratishkaComponent', () => {
  let component: BratishkaComponent;
  let fixture: ComponentFixture<BratishkaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BratishkaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BratishkaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
