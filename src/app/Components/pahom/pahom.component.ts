import { Component, OnInit, Input, Output,EventEmitter  } from '@angular/core';

@Component({
  selector: 'app-pahom',
  templateUrl: './pahom.component.html',
  styleUrls: ['./pahom.component.css']
})
export class PahomComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() Input: string;
  @Output() onChanged = new EventEmitter<boolean>();

  change(increased:any) {
    this.onChanged.emit(increased);
  }

  text:string = "Братишкааааа! Я тебе покушать принес!";

}
