import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PahomComponent } from './pahom.component';

describe('PahomComponent', () => {
  let component: PahomComponent;
  let fixture: ComponentFixture<PahomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PahomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PahomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
